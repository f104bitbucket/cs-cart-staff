<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    fn_trusted_vars('staff_data');
    $suffix = '';

    // multiply
    if (!empty($_REQUEST['staff_ids']) && is_array($_REQUEST['staff_ids'])) {

        // delete
        if ($mode == 'm_delete') {
            foreach ($_REQUEST['staff_ids'] as $staff_id) {
                fn_delete_staff_by_id($staff_id);
            }

            $suffix = '.manage';
        }

        // change status
        if ($mode === 'm_update_statuses' && !empty($_REQUEST['status'])) {
            $status_to = (string) $_REQUEST['status'];

            foreach ($_REQUEST['staff_ids'] as $staff_id) {
                fn_tools_update_status([
                    'table' => 'staffs',
                    'status' => $status_to,
                    'id_name' => 'staff_id',
                    'id' => $staff_id,
                    'show_error_notice' => false
                ]);
            }

            if (defined('AJAX_REQUEST')) {
                $redirect_url = fn_url('staff.manage');
                if (isset($_REQUEST['redirect_url'])) {
                    $redirect_url = $_REQUEST['redirect_url'];
                }
                Tygh::$app['ajax']->assign('force_redirection', $redirect_url);
                Tygh::$app['ajax']->assign('non_ajax_notifications', true);
                return [CONTROLLER_STATUS_NO_CONTENT];
            }
        }
    }
    
    if ($mode == 'm_update') {
        // Update multiple staff data
        foreach ($_REQUEST['staff_data'] as $staff_id => $staff_data) {
            fn_update_staff($staff_data, $staff_id);
        }

        $suffix = ".manage";
    }



    //
    // Add/edit staff
    //
    if ($mode == 'update') {
        $staff_id = fn_update_staff($_REQUEST['staff_data'] ?? [], $_REQUEST['staff_id'] ?? 0);

        $suffix = ".update?staff_id=$staff_id";
    }

    // delete
    if ($mode == 'delete') {
        if (!empty($_REQUEST['staff_id'])) {
            fn_delete_staff_by_id($_REQUEST['staff_id']);
        }

        $suffix = '.manage';
    }

    return array(CONTROLLER_STATUS_OK, 'staff' . $suffix);
}

if ($mode == 'update') {
    $staff = fn_get_staff_data($_REQUEST['staff_id']);

    if (empty($staff)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

    Registry::set('navigation.tabs', array(
        'general' => array(
            'title' => __('general'),
            'js' => true
        ),
    ));

    Tygh::$app['view']->assign('staff', $staff);
} elseif ($mode == 'manage') {

    list($staffs, $params) = fn_get_staffs($_REQUEST, Registry::get('settings.Appearance.admin_elements_per_page'));

    Tygh::$app['view']->assign(array(
        'staffs' => $staffs,
        'search' => $params,
    ));
}
