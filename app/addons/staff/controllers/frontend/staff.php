<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if (isset($_REQUEST['staff_id'])) {
    $staff = fn_get_staff_data($_REQUEST['staff_id']);

    if (empty($staff)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    } else {
        fn_add_breadcrumb(__('staff'), fn_url('staff.view'));
    
        $staff['name'] = $staff['first_name'] . ' ' . $staff['last_name'];
        
        fn_add_breadcrumb($staff['name']);
        
        Tygh::$app['view']->assign(array(
            'staff' => $staff,
            'title' => $staff['name'],
        ));
    }
} else {
    fn_add_breadcrumb(__('staff'));
    
    list($staffs, $params) = fn_get_staffs($_REQUEST, Registry::get('settings.Appearance.elements_per_page'));

    Tygh::$app['view']->assign(array(
        'staffs' => $staffs,
        'search' => $params,
        'title' => __('staff'),
    ));
}