<?php

use Tygh\Tools\SecurityHelper;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

/**
 * Gets staffs list by search params
 *
 * @param array  $params         Search params
 * @param int    $items_per_page Items per page
 *
 * @return array Staffs list and Search params
 */
function fn_get_staffs($params = array(), $items_per_page = 0)
{
    // Set default values to input params
    $default_params = array(
        'page' => 1,
        'items_per_page' => $items_per_page
    );

    $params = array_merge($default_params, $params);

    if (AREA === 'C') {
        $params['status'] = 'A';
    }

    $sortings = array(
        'position' => '?:staffs.position',
        'status' => '?:staffs.status',
        'name' => 'name',
    );

    $condition = $limit = $join = '';

    if (!empty($params['limit'])) {
        $limit = db_quote(' LIMIT 0, ?i', $params['limit']);
    }

    $sorting = db_sort($params, $sortings, 'position', 'asc');

    if (!empty($params['item_ids'])) {
        $condition .= db_quote(' AND ?:staffs.staff_id IN (?n)', explode(',', $params['item_ids']));
    }

    if (!empty($params['name'])) {
        $like = '%' . trim($params['name']) . '%';
        $condition .= db_quote(' AND (?:staffs.first_name LIKE ?l OR ?:staffs.last_name LIKE ?l)', $like, $like);
    }

    if (!empty($params['status'])) {
        $condition .= db_quote(' AND ?:staffs.status = ?s', $params['status']);
    }

    $fields = array(
        '?:staffs.staff_id',
        '?:staffs.status',
        '?:staffs.position',
        'CONCAT_WS(" ", ?:staffs.first_name, ?:staffs.last_name) AS name',
        '?:staffs.post',
        '?:staff_images.staff_image_id',
    );

    if (fn_allowed_for('ULTIMATE')) {
        $fields[] = '?:staffs.company_id';
    }

    /**
     * This hook allows you to change parameters of the staff selection before making an SQL query.
     *
     * @param array        $params    The parameters of the user's query (limit, period, item_ids, etc)
     * @param string       $condition The conditions of the selection
     * @param string       $sorting   Sorting (ask, desc)
     * @param string       $limit     The LIMIT of the returned rows
     * @param string       $lang_code Language code
     * @param array        $fields    Selected fields
     */
    fn_set_hook('get_staffs', $params, $condition, $sorting, $limit, $fields);

    $join .= db_quote(' LEFT JOIN ?:staff_images ON ?:staff_images.staff_id = ?:staffs.staff_id ');

    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:staffs $join WHERE 1 $condition");
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $staffs = db_get_hash_array(
            "SELECT ?p FROM ?:staffs " .
            $join .
            "WHERE 1 ?p ?p ?p",
            'staff_id', implode(', ', $fields), $condition, $sorting, $limit
    );

    if (!empty($params['item_ids'])) {
        $staffs = fn_sort_by_ids($staffs, explode(',', $params['item_ids']), 'staff_id');
    }

    $staff_image_ids = array_column($staffs, 'staff_image_id');
    $images = fn_get_image_pairs($staff_image_ids, 'staff', 'M', true, false);

    foreach ($staffs as $staff_id => $staff) {
        $staffs[$staff_id]['main_pair'] = !empty($images[$staff['staff_image_id']]) ? reset($images[$staff['staff_image_id']]) : array();
    }

    fn_set_hook('get_staffs_post', $staffs, $params);

    return array($staffs, $params);
}

/**
 * Get specific staff data
 * 
 * @param int $staff_id Staff identificator
 * @return array Staff data
 */
function fn_get_staff_data($staff_id)
{
    $fields = array(
        '?:staffs.staff_id',
        '?:staffs.status',
        '?:staffs.position',
        '?:staffs.first_name',
        '?:staffs.last_name',
        '?:staffs.post',
        '?:staffs.email',
        '?:staffs.description',
        '?:staff_images.staff_image_id',
    );

    if (fn_allowed_for('ULTIMATE')) {
        $fields[] = '?:staffs.company_id';
    }

    $joins = [];
    $joins[] = db_quote("LEFT JOIN ?:staff_images ON ?:staff_images.staff_id = ?:staffs.staff_id");

    $condition = db_quote("WHERE ?:staffs.staff_id = ?i", $staff_id);

    if (AREA === 'C') {
        $condition .= db_quote(" AND ?:staffs.status = 'A'");
    }

    /**
     * Prepare params for staff data SQL query
     *
     * @param int   $staff_id  Staff ID
     * @param array $fields    Fields list
     * @param array $joins     Joins list
     * @param str   $condition Conditions query
     */
    fn_set_hook('get_staff_data', $staff_id, $fields, $joins, $condition);

    $staff = db_get_row("SELECT " . implode(", ", $fields) . " FROM ?:staffs " . implode(" ", $joins) . " $condition");

    if (!empty($staff) && !empty($staff['staff_image_id'])) {
        $staff['main_pair'] = fn_get_image_pairs($staff['staff_image_id'], 'staff', 'M', true, false);
    }

    /**
     * Post processing of staff data
     *
     * @param int   $staff_id Staff ID
     * @param array $staff    Staff data
     */
    fn_set_hook('get_staff_data_post', $staff_id, $staff);

    return $staff;
}

/**
 * Hook for deleting store companies
 *
 * @param int $company_id Company id
 */
function fn_staff_delete_company(&$company_id)
{
    if (fn_allowed_for('ULTIMATE')) {
        $staff_ids = db_get_fields("SELECT staff_id FROM ?:staffs WHERE company_id = ?i", $company_id);

        foreach ($staff_ids as $staff_id) {
            fn_delete_staff_by_id($staff_id);
        }
    }
}

/**
 * Deletes staff and all related data
 *
 * @param int $staff_id Staff identificator
 */
function fn_delete_staff_by_id($staff_id)
{
    if (!empty($staff_id)) {
        db_query("DELETE FROM ?:staffs WHERE staff_id = ?i", $staff_id);

        fn_set_hook('delete_staff', $staff_id);

        $image_id = db_get_field("SELECT staff_image_id FROM ?:staff_images WHERE staff_id = ?i", $staff_id);
        if (!empty($image_id)) {
            fn_delete_image_pairs($image_id, 'staff');
        }
        db_query("DELETE FROM ?:staff_images WHERE staff_id = ?i", $image_id);
    }
}

/**
 * Checks of request for need to update the staff image.
 *
 * @return bool
 */
function fn_staff_need_image_update()
{
    if (!empty($_REQUEST['file_staff_main_image_icon']) && is_array($_REQUEST['file_staff_main_image_icon'])) {
        $image = reset($_REQUEST['file_staff_main_image_icon']);

        if ($image == 'staff_main') {
            return false;
        }
    }

    return true;
}

/**
 * Update or create Staff
 * 
 * @param array $data Staff data
 * @param int $staff_id Staff identificator
 * @return int $staff_id
 */
function fn_update_staff($data, $staff_id)
{
    SecurityHelper::sanitizeObjectData('staff', $data);
    $data = array_map('trim', $data);

    $null_fields = ['last_name', 'post', 'email', 'description'];
    foreach ($null_fields as $field) {
        if (empty($data[$field])) {
            $data[$field] = null;
        }
    }

    if (!empty($staff_id)) {
        db_query("UPDATE ?:staffs SET ?u WHERE staff_id = ?i", $data, $staff_id);

        $staff_image_id = fn_get_staff_image_id($staff_id);
        $staff_image_exist = !empty($staff_image_id);
        $image_is_update = fn_staff_need_image_update();

        if ($staff_image_exist && $image_is_update) {
            fn_delete_image_pairs($staff_image_id, 'staff');
            db_query("DELETE FROM ?:staff_images WHERE staff_id = ?i", $staff_id);
            $staff_image_exist = false;
        }

        if ($image_is_update && !$staff_image_exist) {
            $staff_image_id = db_query("INSERT INTO ?:staff_images (staff_id) VALUE(?i)", $staff_id);
            $pair_data = fn_attach_image_pairs('staff_main', 'staff', $staff_image_id);
            $pair_id = reset($pair_data);
            fn_add_image_link($staff_image_id, $pair_id);
        }
    } else {
        $staff_id = $data['staff_id'] = db_query("REPLACE INTO ?:staffs ?e", $data);

        if (fn_staff_need_image_update()) {
            $staff_image_id = db_get_next_auto_increment_id('staff_images');
            $pair_data = fn_attach_image_pairs('staff_main', 'staff', $staff_image_id);
            if (!empty($pair_data)) {
                $pair_id = reset($pair_data);
                fn_add_image_link($staff_image_id, $pair_id);
                $data_staff_image = array(
                    'staff_image_id' => $staff_image_id,
                    'staff_id' => $staff_id,
                );
                db_query("INSERT INTO ?:staff_images ?e", $data_staff_image);
            }
        }
    }

    return $staff_id;
}

/**
 * Get specific staff image identificator
 * 
 * @param int $staff_id Staff identificator
 * @return int Staff image identificator
 */
function fn_get_staff_image_id($staff_id)
{
    return db_get_field("SELECT staff_image_id FROM ?:staff_images WHERE staff_id = ?i", $staff_id);
}

/**
 * Hook for update Staff when deleting image
 */
function fn_staff_delete_image_pre($image_id, $pair_id, $object_type)
{
    if ($object_type == 'staff') {
        $image_data = db_get_row("SELECT staff_id, staff_image_id FROM ?:staff_images INNER JOIN ?:images_links ON object_id = staff_image_id WHERE pair_id = ?i", $pair_id);

        if (!empty($image_data['staff_image_id'])) {
            db_query("DELETE FROM ?:staff_images WHERE staff_image_id = ?i", $image_data['staff_image_id']);
        }
    }
}
