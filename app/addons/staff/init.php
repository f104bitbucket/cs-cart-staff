<?php

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

fn_register_hooks(
    'delete_image_pre'
);

if (fn_allowed_for('ULTIMATE')) {
    fn_register_hooks(
        'delete_company'
    );
}