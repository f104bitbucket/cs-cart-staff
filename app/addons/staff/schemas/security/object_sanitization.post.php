<?php

use Tygh\Tools\SecurityHelper;

$schema['staff'] = array(
    SecurityHelper::SCHEMA_SECTION_FIELD_RULES => array(
        'first_name' => SecurityHelper::ACTION_REMOVE_HTML,
        'last_name' => SecurityHelper::ACTION_REMOVE_HTML,
        'post' => SecurityHelper::ACTION_REMOVE_HTML,
        'email' => SecurityHelper::ACTION_REMOVE_HTML,
        'description' => SecurityHelper::ACTION_SANITIZE_HTML,
    )
);

return $schema;
