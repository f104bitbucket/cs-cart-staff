<?php

$schema['staffs'] = array(
    'controller' => 'staff',
    'mode' => 'update',
    'type' => 'tpl_tabs',
    'params' => array(
        'object_id' => '@staff_id',
        'object' => 'staffs'
    ),
    'table' => array(
        'name' => 'staffs',
        'key_field' => 'staff_id',
    ),
    'request_object' => 'staff_data',
    'have_owner' => true,
);

return $schema;
