{** staff section **}

{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" id="staffs_form" name="staffs_form" enctype="multipart/form-data">
        <input type="hidden" name="redirect_url" value="{$config.current_url}" />

        {include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id="pagination_contents_staffs"}

        {$c_url=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
        {$rev=$smarty.request.content_id|default:"pagination_contents_staffs"}
        {$c_icon="<i class=\"icon-`$search.sort_order_rev`\"></i>"}
        {$c_dummy="<i class=\"icon-dummy\"></i>"}
        {$staff_statuses=""|fn_get_default_statuses:true}
        {$has_permission = fn_check_permissions("update", "admin", "POST")}
        {if $has_permission}
            {$no_hide_input="cm-no-hide-input"}
        {else}
            {$no_hide_input=""}
        {/if}

        {if $staffs}
            {capture name="staffs_table"}
                <div class="table-responsive-wrapper longtap-selection">
                    <table class="table table-middle table--relative table-responsive">
                        <thead
                            data-ca-bulkedit-default-object="true"
                            data-ca-bulkedit-component="defaultObject"
                            >
                            <tr>
                                <th width="6%" class="left mobile-hide">
                                    {include file="common/check_items.tpl" is_check_disabled=!$has_permission check_statuses=$staff_statuses}

                                    <input type="checkbox"
                                           class="bulkedit-toggler hide"
                                           data-ca-bulkedit-disable="[data-ca-bulkedit-default-object=true]"
                                           data-ca-bulkedit-enable="[data-ca-bulkedit-expanded-object=true]"
                                           />
                                </th>
                                <th class="left mobile-hide" width="7%">{__("position_short")}</th>
                                <th></th>
                                <th>
                                    <a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("staff.name")}{if $search.sort_by == "name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
                                </th>

                                {*hook name="staff:manage_header"}
                                {/hook*}

                                <th width="6%" class="mobile-hide">&nbsp;</th>
                                <th width="10%" class="right">
                                    <a class="cm-ajax" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("status")}{if $search.sort_by == "status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a>
                                </th>
                            </tr>
                        </thead>
                        {foreach from=$staffs item=staff}
                            <tr class="cm-row-status-{$staff.status|lower} cm-longtap-target"
                                {if $has_permission}
                                    data-ca-longtap-action="setCheckBox"
                                    data-ca-longtap-target="input.cm-item"
                                    data-ca-id="{$staff.staff_id}"
                                {/if}
                                >

                                <td width="6%" class="left mobile-hide">
                                    <input type="checkbox" name="staff_ids[]" value="{$staff.staff_id}" class="cm-item {$no_hide_input} cm-item-status-{$staff.status|lower} hide" />
                                </td>
                                <td width="7%" class="mobile-hide" data-th="{__("position_short")}">
                                    {if $has_permission}
                                        <input type="text" name="staff_data[{$staff.staff_id}][position]" size="3" maxlength="10" value="{$staff.position}" class="input-micro input-hidden" />
                                    {else}
                                        {$staff.position}
                                    {/if}
                                </td>
                                <td width="{$settings.Thumbnails.product_admin_mini_icon_width}">
                                    {include 
                                        file="common/image.tpl" 
                                        image=$staff.main_pair.icon|default:$staff.main_pair.detailed 
                                        image_id=$staff.main_pair.image_id 
                                        image_width=$settings.Thumbnails.product_admin_mini_icon_width 
                                        image_height=$settings.Thumbnails.product_admin_mini_icon_height 
                                        href="staff.update?staff_id=`$staff.staff_id`"|fn_url
                                    }
                                </td>
                                <td class="{$no_hide_input}" data-th="{__("staff.name")}">
                                    <a class="row-status" href="{"staff.update?staff_id=`$staff.staff_id`"|fn_url}">{$staff.name}</a>
                                    {include file="views/companies/components/company_name.tpl" object=$staff}
                                </td>

                                <td width="6%" class="mobile-hide">
                                    {capture name="tools_list"}
                                        <li>{btn type="list" text=__("edit") href="staff.update?staff_id=`$staff.staff_id`"}</li>
                                        <li>{btn type="list" class="cm-confirm" text=__("delete") href="staff.delete?staff_id=`$staff.staff_id`" method="POST"}</li>
                                    {/capture}
                                    <div class="hidden-tools">
                                        {dropdown content=$smarty.capture.tools_list}
                                    </div>
                                </td>
                                <td width="10%" class="right" data-th="{__("status")}">
                                    {include file="common/select_popup.tpl" id=$staff.staff_id status=$staff.status hidden=true object_id_name="staff_id" table="staffs" popup_additional_class="`$no_hide_input` dropleft"}
                                </td>
                            </tr>
                        {/foreach}
                    </table>
                </div>
            {/capture}

            {include file="common/context_menu_wrapper.tpl"
                form="staffs_form"
                object="staff"
                items=$smarty.capture.staffs_table
                has_permissions=$has_permission
            }
        {else}
            <p class="no-items">{__("no_data")}</p>
        {/if}

        {include file="common/pagination.tpl" div_id="pagination_contents_staffs"}

    </form>

{/capture}

{capture name="buttons"}
    {hook name="staff:buttons"}
        {if $staffs}
            {include file="buttons/save.tpl" but_name="dispatch[staff.m_update]" but_role="action" but_target_form="staffs_form" but_meta="cm-submit"}
        {/if}
    {/hook}
{/capture}

{capture name="adv_buttons"}
    {hook name="staff:adv_buttons"}
        {include file="common/tools.tpl" tool_href="staff.add" prefix="top" hide_tools="true" title=__("staff.staff_add") icon="icon-plus"}
    {/hook}
{/capture}

{capture name="sidebar"}
    {hook name="staff:manage_sidebar"}
    {include file="addons/staff/views/staff/components/search_form.tpl" dispatch="staff.manage"}
    {/hook}
{/capture}

{hook name="staff:manage_mainbox_params"}
    {$page_title = __("staff")}
{/hook}

{include file="common/mainbox.tpl" 
    title=$page_title 
    content=$smarty.capture.mainbox 
    buttons=$smarty.capture.buttons 
    adv_buttons=$smarty.capture.adv_buttons 
    sidebar=$smarty.capture.sidebar}
