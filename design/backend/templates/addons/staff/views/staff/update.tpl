{if $staff}
    {$id=$staff.staff_id}
{else}
    {$id=0}
{/if}


{** staff section **}

{$has_permission = fn_check_permissions("update", "admin", "POST")}
{$hide_inputs = ""|fn_check_form_permissions}

{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" class="form-horizontal form-edit{if !$has_permission || $hide_inputs} cm-hide-inputs{/if}" name="staff_form" enctype="multipart/form-data">
        <input type="hidden" class="cm-no-hide-input" name="staff_id" value="{$id}" />

        {capture name="tabsbox"}

            <div id="content_general">
                <div class="control-group">
                    <label for="elm_staff_first_name" class="control-label cm-required">{__("staff.first_name")}</label>
                    <div class="controls">
                        <input type="text" name="staff_data[first_name]" id="elm_staff_first_name" value="{$staff.first_name}" class="input-large" required />
                    </div>
                </div>
                <div class="control-group">
                    <label for="elm_staff_last_name" class="control-label">{__("staff.last_name")}</label>
                    <div class="controls">
                        <input type="text" name="staff_data[last_name]" id="elm_staff_last_name" value="{$staff.last_name}" class="input-large" />
                    </div>
                </div>
                <div class="control-group">
                    <label for="elm_staff_post" class="control-label">{__("staff.post")}</label>
                    <div class="controls">
                        <input type="text" name="staff_data[post]" id="elm_staff_post" value="{$staff.post}" class="input-large" />
                    </div>
                </div>
                <div class="control-group">
                    <label for="elm_staff_email" class="control-label">{__("staff.email")}</label>
                    <div class="controls">
                        <input type="email" name="staff_data[email]" id="elm_staff_email" value="{$staff.email}" class="input-large" />
                    </div>
                </div>
                <div class="control-group">
                    <label for="elm_staff_description" class="control-label">{__("staff.description")}</label>
                    <div class="controls">
                        <textarea id="elm_page_description"
                            name="staff_data[description]"
                            class="cm-wysiwyg input-large"
                        >{$staff.description}</textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label for="elm_staff_position" class="control-label">{__("position")}</label>
                    <div class="controls">
                        <input type="text" name="staff_data[position]" id="elm_staff_position" value="{$staff.position|default:"0"}" size="3" />
                    </div>
                </div>

                {if "ULTIMATE"|fn_allowed_for}
                    {include file="views/companies/components/company_field.tpl"
                        name="staff_data[company_id]"
                        id="staff_data_company_id"
                        selected=$staff.company_id
                    }
                {/if}


                <div class="control-group" id="staff_graphic">
                    <label class="control-label">{__("image")}</label>
                    <div class="controls">
                        {include file="common/attach_images.tpl"
                            image_name="staff_main"
                            image_object_type="staff"
                            image_pair=$staff.main_pair
                            image_object_id=$id
                            no_detailed=true
                            hide_titles=true
                            hide_alt=true
                        }
                    </div>
                </div>

                {include file="common/select_status.tpl" input_name="staff_data[status]" id="elm_staff_status" obj_id=$id obj=$staff hidden=true}
            
            </div>

        {/capture}
        
        {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}

        {capture name="buttons"}
            {include file="buttons/button.tpl" but_text=__("close") but_role="action" but_href="staff.manage"}
            {include file="buttons/save_cancel.tpl" but_text=__("save") but_role="submit-link" but_target_form="staff_form" but_name="dispatch[staff.update]"}
            {*if !$id}
            {else}
                {*if "ULTIMATE"|fn_allowed_for && !$has_permission}
                    {assign var="hide_first_button" value=true}
                    {assign var="hide_second_button" value=true}
                {/if}
                {include file="buttons/save_cancel.tpl" but_name="dispatch[staff.update]" but_role="submit-link" but_target_form="staff_form" hide_first_button=$hide_first_button hide_second_button=$hide_second_button save=$id}
            {/if*}
        {/capture}

    </form>

{/capture}

{include file="common/mainbox.tpl"
    title=($id) ? __("staff.staff_edit") : __("staff.staff_new")
    content=$smarty.capture.mainbox
    buttons=$smarty.capture.buttons}

{** staff section **}
