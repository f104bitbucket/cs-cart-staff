{if $staff}
    <div class="ty-product-block ty-product-detail">
        <div class="ty-product-block__wrapper clearfix">
            {if $staff.main_pair}
                <div class="ty-product-block__img-wrapper" _style="width:{$settings.Thumbnails.product_details_thumbnail_width}">
                    <div class="ty-product-img">
                        {include 
                        file="common/image.tpl" 
                        images=$staff.main_pair 
                        image_width=$settings.Thumbnails.product_details_thumbnail_width|default:280 
                        image_height=$settings.Thumbnails.product_details_thumbnail_height|default:280
                        }
                    </div>
                </div>
            {/if}
            <div class="ty-product-block__left">
                <h1 class="ty-product-block-title">{$title}</h1>
                <div class="ty-product-block__sku">
                    {if $staff.post}
                        <div class="ty-control-group ty-sku-item">
                            <label class="ty-control-group__label">{__("staff.post")}:</label>
                            <span class="ty-control-group__item">{$staff.post}</span>
                        </div>
                    {/if}
                    {if $staff.email}
                        <div class="ty-control-group ty-sku-item">
                            <label class="ty-control-group__label">{__("staff.email")}:</label>
                            <span class="ty-control-group__item">
                                <a href="mailto:{$staff.email}">{$staff.email}</a>
                            </span>
                        </div>
                    {/if}
                </div>
                {$staff.description nofilter}
            </div>
        </div>
    </div>

{else}
    <h1 class="ty-mainbox-title">{$title}</h1>
    {if $staffs}
        {include file="common/pagination.tpl" id="pagination_contents" search=$search}
        <div class="ty-mainbox-body">
            <div class="ty-pagination-container cm-pagination-container" id="pagination_contents">

                <div class="ty-compact-list">
                    {foreach from=$staffs item=staff}
                        {$staff_link="staff.view?staff_id=`$staff.staff_id`"|fn_url}
                        <div class="ty-compact-list__item">
                            <div class="ty-compact-list__content">
                                <div class="ty-compact-list__image">
                                    <a href="{$staff_link}">
                                        {include 
                                            file="common/image.tpl" 
                                            images=$staff.main_pair 
                                            image_width=$settings.Thumbnails.category_lists_thumbnail_width|default:60 
                                            image_height=$settings.Thumbnails.category_lists_thumbnail_height|default:60
                                            href="staff.view?staff_id=`$staff.staff_id`"|fn_url
                                        }
                                    </a>
                                </div>
                                <div class="ty-compact-list__title">
                                    <a href="{$staff_link}" class="product-title">
                                        {$staff.name}
                                    </a>
                                    <div class="ty-control-group">
                                        <span class="ty-control-group__item">{$staff.post}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {/foreach}
                </div>

            </div>

            {include file="common/pagination.tpl" id="pagination_contents" search=$search}

        </div>
    {else}
        <p>{__("no_data")}</p>
    {/if}
{/if}